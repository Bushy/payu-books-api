package com.payu.books.util;


import com.payu.books.persistance.entity.Book;

import java.math.BigDecimal;
import java.util.Date;

public class TestUtil {
    public static Book buildBook() {
        return Book.builder()
                .id(1L)
                .name("first Book")
                .isbnNumber("1234")
                .price(BigDecimal.TEN)
                .build();
    }
}
