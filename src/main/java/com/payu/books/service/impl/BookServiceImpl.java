package com.payu.books.service.impl;

import com.payu.books.exception.BookNotFoundException;
import com.payu.books.persistance.entity.Book;
import com.payu.books.persistance.repository.BookRepository;
import com.payu.books.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.StreamSupport.stream;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @Override
    public Book save(Book bookRequest) {
        validateRequest(bookRequest);
        log.info("bookService.save entered with request : {} ", bookRequest);
        Book bookResponse = bookRepository.save(bookRequest);
        log.info("save success response: {}", bookResponse);
        return bookResponse;
    }

    @Override
    public Book update(Book bookRequest, Long id) {
        validateRequest(bookRequest);
        log.info("bookService.update entered with request : {} and id:{}", bookRequest, id);
        bookRequest.setId(id);
        validateRequest(bookRequest);
        Book bookResponse = bookRepository.save(bookRequest);
        log.info("update success response: {}", bookResponse);
        return bookResponse;
    }

    @Override
    public Book findById(Long id) {
        log.info("bookService.findById entered with id : {} ", id);
        Optional<Book> book = bookRepository.findById(id);
        log.info("completed book: {}", book);
        return book.orElseThrow(() -> new BookNotFoundException("book not found with id: " + id));
    }

    @Override
    public boolean existsById(Long id) {
        log.info("bookService.existsById entered with id : {} ", id);
        boolean exists = bookRepository.existsById(id);
        log.info("exists: {}", exists);
        return exists;
    }

    @Override
    public List<Book> findAll() {
        log.info("bookService.findAll entered");
        Iterable<Book> books = bookRepository.findAll();

        List<Book> bookResponses = stream(books.spliterator(), false)
                .sorted(Comparator
                        .comparing(Book::getPublishedDate)
                        .thenComparing(Book::getName)
                        .reversed())
                .collect(Collectors.toList());
        if (bookResponses.isEmpty()) {
            log.error("find all books returned empty list ");
            throw new BookNotFoundException("books not found");
        }
        log.info("findAll success response:{}", bookResponses);
        return bookResponses;
    }

    @Override
    public long count() {
        log.info("bookService.count entered");
        long count = bookRepository.count();
        log.info("count completed:{}", count);
        return count;
    }

    @Override
    public void deleteById(Long id) {
        log.info("bookService.deleteById entered");
        bookRepository.deleteById(id);
        log.info("deleteById completed");
    }

    private void validateRequest(Book bookRequest) {
        Assert.notNull(bookRequest, "book request cannot be null");
        Assert.hasText(bookRequest.getName(), "name cannot be empty");
        Assert.notNull(bookRequest.getPrice(), "price cannot be empty");
    }
}
