package com.payu.books.service;

import com.payu.books.persistance.entity.Book;

import java.util.List;

public interface BookService {

    Book save(Book book);

    Book update(Book bookRequest, Long id);

    Book findById(Long id);

    boolean existsById(Long id);

    List<Book> findAll();

    long count();

    void deleteById(Long id);
}
