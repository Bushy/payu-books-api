package com.payu.books;

import com.payu.books.persistance.entity.Book;
import com.payu.books.persistance.entity.BookType;
import com.payu.books.service.BookService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
@OpenAPIDefinition(info =
@Info(title = "PayU Books API", version = "v1.0"))
public class PayuBooksApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PayuBooksApiApplication.class, args);
	}

	@Bean
		//init inmemory Data for test
	ApplicationRunner applicationRunner(BookService bookService){
		return args -> bookService.save(Book.builder()
				.name("first Book")
				.isbnNumber("1234")
				.price(BigDecimal.TEN)
				.publishedDate(new Date())
				.type(BookType.EBOOK)
				.build());
	}

}
