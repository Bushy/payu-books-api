package com.payu.books.persistance.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
@Entity
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "BOOK")
public class Book {
    @Id
    @GeneratedValue
    @ToString.Exclude
    @Column(name = "ID")
    private Long id;
    @Column(name = "TITLE")
    private String name;
    @Column(name = "ISBN_NUMBER")
    private String isbnNumber;
    private BookType type;
    private BigDecimal price;
    @Column(name = "PUBLISHED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date publishedDate;

    @PrePersist
    private void prePersist(){
        publishedDate = new Date();
    }

}
