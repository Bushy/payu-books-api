package com.payu.books.persistance.entity;

public enum BookType {
    HARD_COVER, SOFT_COVER, EBOOK, OTHER
}
