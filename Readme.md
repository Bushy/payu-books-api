# PayU RESTful library API

### Functional Requirement

- List
- Add
- Update
- Delete


> API documentation available at http://localhost:8080/payu-books-api/swagger-ui/index.html#/ after starting the app
   ![img.png](img.png)
> 

### Pre-Requirements
- [jdk1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-5066655.html)
- [maven](https://maven.apache.org/)

## Tech stack
- Spring Boot 2 (web)
- H2 inMemory DB


### Test
- `mvn test`

### Build 
- `mvn clean install`

### Start app
- `mvn spring-boot:run`

>UNIT tests can be found under src/test
> ![img_1.png](img_1.png)

### Decisions
I opted to use H2 inmemory DB mainly because it is quick to setup and does not require db installation to start 

I also used SOLID principles and good code patterns, and for unit tests I decided to use mainly mockito without spring runner for fast test startup.


### Future Steps
- Use real cloud DB
- Apply CI/CD pipeline